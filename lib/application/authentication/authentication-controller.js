const tokenService = require('../../services/token-service')
const usecases = require('../../domain/usecases')

module.exports = {
  async loginUser (request, h) {
    const { email, password } = request.payload

    try {
      const user = await usecases.logUserIn({ email, password })
      const token = await tokenService.getTokenFromUser(user)
      return {
        token
      }
    } catch (err) {
      return h.response(err).notFound()
    }
  }
}
