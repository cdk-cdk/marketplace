const authenticationController = require('./authentication-controller')

exports.register = async function (server) {
  server.route([
    {
      method: 'POST',
      path: '/api/v1/authentication/login',
      config: {
        handler: authenticationController.loginUser,
        tags: ['api', 'authentication'],
        notes: [
          '- **Cette route permet de login un nouvel utilisateur**\n' +
          '- En cas de succès, renvoie un tokenSecret signé avec les informations de l\'utilisateur'
        ]
      }
    }
  ])
}

exports.name = 'authentication-api'
