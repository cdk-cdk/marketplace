const healthcheckController = require('./healthcheck-controller')

exports.register = async function (server) {
  server.route([
    {
      method: 'GET',
      path: '/api/v1/status',
      config: {
        handler: healthcheckController.checkApiStatus,
        tags: ['api', 'healthy'],
        notes: [
          '- **Cette route permet de s\'assurer que l\'api est up and running**\n' +
          '- Renvoie le statut de l\'api'
        ]
      }
    }
  ])
}

exports.name = 'healthcheck-api'
