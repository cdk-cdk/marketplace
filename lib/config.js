const ONE_HOUR_IN_SECONDS = 3600

function getConfig () {
  const baseConfiguration = {
    now: new Date(),
    api: {
      tokenSecret: process.env.API_TOKEN_SECRET,
      tokenLifespanInSeconds: process.env.API_TOKEN_LIFESPAN_IN_SECONDS,
    }
  }
  if (process.env.NODE_ENV === 'test') {
    return {
      ...baseConfiguration,
      ...{
        now: new Date('2010-10-10'),
        api: {
          tokenSecret: 'dragon ball z',
          tokenLifespanInSeconds: ONE_HOUR_IN_SECONDS
        }
      }
    }
  }
}

module.exports = {
  getConfig
}
