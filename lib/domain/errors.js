class NotFoundError extends Error {
  constructor (message = 'Resource not found') {
    super(message)
  }
}

module.exports = {
  NotFoundError
}
