const _ = require('lodash')

function injectDefaults (defaults, targetFn) {
  return (args) => targetFn(Object.assign(Object.create(defaults), args))
};

const dependencies = {
  userRepository: require('../../infrastructure/repositories/user-repository')
}

function injectDependencies (usecases) {
  return _.mapValues(usecases, _.partial(injectDefaults, dependencies))
}

module.exports = injectDependencies({
  logUserIn: require('./log-user-in')
})
