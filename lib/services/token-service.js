const { getConfig } = require('../config')
const jsonwebtoken = require('jsonwebtoken')

function getTokenFromUser (user) {
  const secret = getConfig().api.tokenSecret
  const expiresInSeconds = getConfig().api.tokenLifespanInSeconds

  return createToken({ userId: user.id, secret, expiresInSeconds })
}

function createToken ({ userId, secret, expiresInSeconds }) {
  const nowInSeconds = Math.floor(getConfig().now / 1000)
  return jsonwebtoken.sign({
    exp: nowInSeconds + expiresInSeconds,
    data: { userId }
  }, secret)
}

function decodeToken (token, secret) {
  return jsonwebtoken.verify(token, secret)
}

module.exports = {
  createToken,
  decodeToken,
  getTokenFromUser
}
