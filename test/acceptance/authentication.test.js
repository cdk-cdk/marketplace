'use strict'
const { getConfig } = require('../../lib/config')
const jsonwebtoken = require('jsonwebtoken')
const sinon = require('sinon')
const createServer = require('../../lib/server')

describe('Authentication', () => {
  let server

  const apiToken = 'some token'
  beforeEach(async () => {
    server = await createServer()
    const nowInSeconds = Math.floor(getConfig().now / 1000)

    sinon.stub(jsonwebtoken, 'sign')
      .withArgs({
        exp: nowInSeconds + getConfig().api.tokenLifespanInSeconds,
        data: { userId: 42 }
      }, 'dragon ball z')
      .returns(apiToken)
  })
  afterEach(async () => {
    sinon.restore()
    await server.stop()
  })

  describe('POST /api/v1/auth/login', () => {
    it('should respond with 200 with a token when the user exists', async () => {
      // when
      const res = await server.inject({
        method: 'post',
        url: '/api/v1/authentication/login',
        payload: {
          email: 'gogeta@dbz.com',
          password: 'supersayan'
        }
      })

      // then
      expect(res.statusCode).toEqual(200)
      expect(res.result.token).toEqual(apiToken)
    })
  })
})
