'use strict'
const createServer = require('../../lib/server')

describe('Healthcheck', () => {
  let server

  beforeEach(async () => {
    server = await createServer()
  })
  afterEach(async () => {
    await server.stop()
  })

  describe('GET /api/v1/status', () => {
    it('should respond with 200 and api version', async () => {
      const res = await server.inject({
        method: 'get',
        url: '/api/v1/status'
      })
      expect(res.statusCode).toEqual(200)
      expect(res.result.version).toEqual('1')
    })
  })

  describe('GET /api/v1/fake-url', () => {
    it('should respond with 404 when the route does not exist', async () => {
      const res = await server.inject({
        method: 'get',
        url: '/fake-url'
      })
      expect(res.statusCode).toEqual(404)
    })
  })
})
