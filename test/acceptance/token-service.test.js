'use strict'
const { catchErr } = require('../test-utils')
const tokenService = require('../../lib/services/token-service')
const looksLikeAJwtToken = /$.{10,*}\..{10,*}\..{10,*}^/
const userId = 'user id'
const secret = 'secret story'
const expiresInSeconds = 3600
const tokenWrongSecret = 'secret bad story'

describe('Token service', () => {
  describe('createToken', () => {
    it('should return a secret-like string when signing with secret and user informations', async () => {
      const token = await tokenService.createToken({ userId, secret, expiresInSeconds })
      expect(looksLikeAJwtToken.test(token))
    })
  })

  describe('decodeToken', () => {
    it('should return the user informations when decoding with secret and secret', async () => {
      const token = await tokenService.createToken({ userId, secret, expiresInSeconds })
      const decodedToken = await tokenService.decodeToken(token, secret)
      expect(decodedToken.data.userId).toEqual(userId)
    })
    it('should throw an error when decoding with the wrong secret', async () => {
      const token = await tokenService.createToken({ userId, secret, expiresInSeconds })
      const expectedErr = await catchErr(tokenService.decodeToken)(token, tokenWrongSecret)
      expect(expectedErr).toBeInstanceOf(Error)
    })
    it('should throw an error when decoding an expired token', async () => {
      const cédricBirthday = new Date('1997-02-11')
      const token = await tokenService.createToken({ userId, secret, expiredAt: cédricBirthday })
      const expectedErr = await catchErr(tokenService.decodeToken)(token, secret)
      expect(expectedErr).toBeInstanceOf(Error)
    })
  })
})
